'use strict';
const Auth = require('../services/auth');


(async () => {
  const auth = new Auth();
  auth.init({
    baseURL: 'http://localhost:1005/',
    headers: { 'Cache-Control': 'no-cache' },
    timeout: 5000
  });
  // const users = await auth.find();
  // const user = await auth.findOne({id: 3});
  // const user = await auth.findOneUuid({ uuid: '199008026'});
  // const user = await auth.findOneDetails({ uuid: '199008026'});
  // const user = await auth.authLocal({ identifier: '199008026', password: '1234567'});
  const user = await auth.userDetailMe(null, {
    headers: {
      Authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IjE5OTAwODAyNiIsImlkIjo0LCJlbWFpbCI6ImFuaGtob2EubGVAYml0aXMuY29tLnZuIiwicm9sZXMiOlsicHVibGljIiwic2hpZnRsZWFkZXIiLCJzdGFmZiJdLCJwcm92aWRlciI6ImxvY2FsIiwiaWF0IjoxNTc2MTIyMjkzLCJleHAiOjE1Nzg3MTQyOTN9.3J04VHBLFaNAz6BvacrDNfriaHRIp2o6t2M8fBT1-LY"
    }
  });
  console.log("DebugLog: user", user)
})()




