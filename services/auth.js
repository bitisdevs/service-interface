
'use strict';
const _ = require('lodash');
const axios = require('axios');
const apisauce = require('apisauce');
const { formatResponse } = require('../utils');



module.exports = class {
  constructor() {
    this.baseURL = null
    this.headers = null
    this.api = null
  }

  init({ baseURL, headers, timeout }) {
    this.api = apisauce.create({ baseURL, headers, timeout });
  }

  find(params, requestConfig) {
    if (!this.api) throw new Error('the services need initialize');
    return this.api.get('/users', { ...params }, requestConfig)
      .then(res => formatResponse(res))
  }

  async findOne(params, requestConfig) {
    const { id } = params;
    if (!this.api) throw new Error('the services need initialize');
    return this.api.get(`/users/${id}`, { ..._.omit(params, ['id']) }, requestConfig)
      .then(res => formatResponse(res))
  }

  async findOneUuid(params, requestConfig) {
    const { uuid } = params;
    if (!this.api) throw new Error('the services need initialize');
    return this.api.get(`/users/findOne/${uuid}`, { ..._.omit(params, ['uuid']) }, requestConfig)
      .then(res => formatResponse(res))
  }

  async findOneDetails(params, requestConfig) {
    const { uuid } = params;
    if (!this.api) throw new Error('the services need initialize');
    return this.api.get(`/userDetails/findOne/${uuid}`, { ..._.omit(params, ['uuid']) }, requestConfig)
      .then(res => formatResponse(res))
  }

  async authLocal(params, requestConfig) {
    const { identifier, password } = params;
    if (!this.api) throw new Error('the services need initialize');
    return this.api.post('/auth/local', {
      identifier,
      password
    }, requestConfig).then(res => formatResponse(res));
  }

  async userMe(params, requestConfig) {
    if (!this.api) throw new Error('the services need initialize');
    return this.api.get('/users/me', {}, requestConfig).then(res => formatResponse(res));
  }

  async userDetailMe(params, requestConfig) {
    if (!this.api) throw new Error('the services need initialize');
    return this.api.get('/userDetails/me', {}, requestConfig).then(res => formatResponse(res));
  }


}


