


module.exports = {
  formatResponse: (res) => {
    if (res.ok) return res.data
    if (!res.ok && res.status === null) {
      return {
        "statusCode": 404,
        code: res.problem,
        error: JSON.stringify(res.originalError)
      }
    }
    return res.data
  }
}